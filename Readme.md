Pages "médiation scientifique en informatique" du portail
=========================================================


http://portail.fil.univ-lille1.fr/mediation/

* `src/` les fichiers sources, Markdown
* `portail/` les fichiers exposés sur le portail
  - fichiers générés depuis les `../src/*`
  - fichiers médias dans `portail/medias/`

  
* `Makefile` pour contruire `portail/` à partir de `src`   
  - les fichers générés doivent être poussés sur le git
  - le portail inclut les fichiers de la branche `master` du dépôt


