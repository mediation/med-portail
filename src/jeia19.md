5e Journée EIA, mars 2019
-------------------------
 
Journée enseignement de l'informatique et l'algorithmique, mercredi
6 mars 2019

Une JEAI spéciale IA !

Informations et inscriptions sur
[jeia.fil.univ-lille1.fr](http://jeia.fil.univ-lille1.fr)

::: signature
Philippe MARQUET \
[portail.fil.univ-lille1.fr/mediation/](http://portail.fil.univ-lille1.fr/mediation/)
:::
