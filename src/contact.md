Contacts
--------

* Philippe Marquet  
  [philippe.marquet@univ-lille.fr](philippe.marquet@univ-lille.fr)  
  Bureau 225, 2e étage de l'extension du bâtiment M3  
  [coordonnées](https://www.cristal.univ-lille.fr/profil/marquet)

::: signature
Philippe MARQUET \
[portail.fil.univ-lille1.fr/mediation/](http://portail.fil.univ-lille1.fr/mediation/)
:::
