Médiation scientifique en informatique
--------------------------------------

0110100101111001... La médiation scientifique en informatique

Vous pouvez contacter :

* [Philippe Marquet](https://www.cristal.univ-lille.fr/profil/marquet)

De nombreux enseignants du département informatique - FIL -
participent à ces actions de médiation, ou sont intéressés par cette
question. En particulier Laetitia Jourdan et Philippe Marquet ont
encadré les étudiants qui ont mis en place les "coding goûters" pour
les enfants des personnels d'Inria, de l'université et l'animation
tout public des journées portes ouvertes. Philippe Marquet a initié
les JEIA. Pierre Boulet, Francesco de Comité, Didier Mailliet,
Philippe Marquet, Maude Pupin, Jean-Christophe Routier, Jean-Stéphane
Varré, Marie-Emilie Voge, et Éric Wegrzynowski ont contribué aux
formations des enseignants d'ISN. etc.

::: signature
Philippe MARQUET \
[portail.fil.univ-lille1.fr/mediation/](http://portail.fil.univ-lille1.fr/mediation/)
:::
