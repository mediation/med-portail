ISN - Informatique et science du numérique
------------------------------------------
 
Nous avons assuré la formation des enseignants de la spécialité ISN,
_Infomatique et science du numérique_, de l'académie de Lille.

À cette occasion, nous avons produit un ensemble de propositions
d'activités à mener avec les élèves qui couvrent certains points du
programme de la spécialité. Retrouvez ces propositions sur

* [http://isn.fil.univ-lille1.fr/](http://isn.fil.univ-lille1.fr/)

::: signature
Philippe MARQUET \
[portail.fil.univ-lille1.fr/mediation/](http://portail.fil.univ-lille1.fr/mediation/)
:::
