<h2 id="informatique-débranchée-journée-portes-ouvertes-janvier-2017">Informatique débranchée – Journée Portes Ouvertes janvier 2017</h2>
<p>Lors de l’édition 2017 de la Journée Portes Ouvertes de l’Université Lille 1, des étudiants en informatique ont eu l’occasion d’organiser un atelier « Informatique débranchée ». Cet atelier a permis de faire découvrir certaines notions clés de la science informatique au public, de façon ludique, et sans utiliser d’ordinateur.</p>
<p>Ci-dessous sont détaillées les différentes activités proposées à l’occasion de cet atelier.</p>
<h3 id="le-crêpier-psycho-rigide">Le crêpier psycho-rigide</h3>
<p><strong>Nombre de joueur(s)</strong> : 1</p>
<p><strong>Matériel nécessaire :</strong> cinq planchettes de taille croissante (Les « crêpes »). Les planchettes disposent d’une face colorée, et une face blanche.</p>
<p><strong>But du jeu :</strong> le joueur est un crêpier qui doit ranger ses crêpes en fonction de leur taille, la plus grande devant être en bas et la plus petite en haut.</p>
<p><strong>Règles du jeu :</strong></p>
<ul>
<li>les planchettes sont des crêpes qu’il faut ranger de la plus grande à la plus petite ;</li>
<li>à chaque coup, il faut choisir une crêpe, puis retourner le haut de la pile, c’est-à-dire le bloc formé par cette crêpe et toutes celles placées au-dessus ;</li>
<li>il est impossible de poser des crêpes sur le côté (il n’y a qu’un seul et même tas de crêpes). Il est aussi impossible de soulever des crêpes pour toucher au milieu de la pile.</li>
</ul>
<p><strong>Solution (Algorithme du crêpier) :</strong> il faut découper le problème en plusieurs petits problèmes. Dans un premier temps, le joueur doit amener la plus grande crêpe en bas. Pour faire cela, il faut :</p>
<ol type="1">
<li>sélectionner la plus grande crêpe, et retourner le bloc associé. La plus grande crêpe se retrouve donc en haut de la pile ;</li>
<li>une fois la plus grande crêpe en haut de pile, il suffit de retourner le tas de crêpe entier. La plus grande crêpe est donc tout en bas du tas ;</li>
<li>le joueur peut maintenant répéter l’opération sur la partie du tas qui n’est pas triée (toucher aux crêpes déjà triées serait contre-productif).</li>
</ol>
<p><strong>Variante (plus dur) :</strong> ajout d’une contrainte : les faces colorées des crêpes doivent être orientées vers le haut.</p>
<p><strong>Lien avec l’informatique :</strong> pour résoudre ce genre de problèmes, un ordinateur va suivre des ordres précis et ordonnés (la solution présentée plus haut en est un bon exemple). Cet ensemble d’ordres (ou instructions) s’appelle un algorithme.<br />
L’intérêt de cette activité est de démontrer que n’importe qui peut mettre au point un algorithme, et que certains concepts informatiques sont trouvables dans des domaines qui n’y sont pas directement liés.</p>
<iframe width="560" height="315" src="https://www.youtube.com/embed/tI6uTAlX-_w" frameborder="0" allowfullscreen>
</iframe>
<h3 id="la-traversée-de-la-rivière">La traversée de la rivière</h3>
<p><strong>Nombre de joueur(s) :</strong> 1</p>
<p><strong>Matériel nécessaire :</strong> de quoi représenter quatre personnages, un pont, et une torche (des jouets pour enfant suffisent). Papiers et crayons peuvent aider à la réflexion.</p>
<p><strong>Introduction :</strong> en pleine nuit, un aventurier et ses acolytes tentent de fuir les ennemis qui les pourchassent. Dans leur fuite, ils rencontrent un pont suspendu au-dessus du vide. Le pont en question est très ancien, et par conséquent fragile et troué de toutes parts. Par chance, le groupe comprend une aventurière, qui connait la forêt comme sa poche. Cette dernière donne plusieurs indications afin de permettre au groupe de traverser le pont sain et sauf.</p>
<p><strong>But du jeu :</strong> les 4 personnages doivent arriver de l’autre côté du pont. Il faut donc trouver un ensemble de voyages permettant d’arriver à ce résultat.</p>
<p><strong>Règles du jeu :</strong></p>
<ul>
<li>les 4 personnages traversent le pont à des vitesses différentes :
<ol type="1">
<li>l’aventurière : elle connait la forêt comme sa poche et prend 5 minutes pour franchir le pont ;</li>
<li>l’aventurier : il a une excellente condition physique mais n’est pas aussi confiant que l’aventurière, il met 10 minutes pour franchir le pont ;</li>
<li>l’infirmier : il a une condition physique dans la moyenne, il met donc 20 minutes pour franchir le pont ;</li>
<li>l’archéologue : elle est déterminée et sportive, mais sa petite taille rend la traversée du pont plus délicate. Elle met 25 minutes pour le traverser ;</li>
</ol></li>
<li>le pont ne peut pas supporter plus de 2 personnes par voyage ;</li>
<li>la torche est indispensable pour chaque voyage, aller comme retour ;</li>
<li>si 2 personnes traversent simultanément, la personne la plus rapide devant accompagner la plus lente, le temps d’une traversée sera celui de la personne la plus lente (par exemple : si l’aventurier et l’archéologue traversent ensemble, le temps sera de 25 minutes) ;</li>
<li>les personnages peuvent se passer la torche entre eux.</li>
</ul>
<p><strong>Solution intuitive :</strong> une solution intuitive serait de faire passer chaque personne avec l’aventurière. Cette dernière ferait en permanence l’aller-retour afin de ramener la torche et accompagner les autres membres du groupe. Voici un des déroulements possibles de cette solution :</p>
<ol type="1">
<li>aller : aventurière + aventurier = 10 minutes ;</li>
<li>retour : aventurière = 5 minutes ;</li>
<li>aller : aventurière + médecin = 20 minutes ;</li>
<li>retour : aventurière = 5 minutes ;</li>
<li>aller : aventurière + archéologue = 25 minutes.</li>
</ol>
<p>Nous obtenons donc un total de 10 + 5 + 20 + 5 + 25 = 65 minutes.  Cette solution est motivée par l’idée que l’aventurière est la plus rapide à traverser le pont, par conséquent, si c’est elle qui tient en permanence la torche, le temps pris pour les voyages retours est optimisé.</p>
<p><strong>Après avoir trouvé une solution :</strong> les ennemis du groupe mettent 64 minutes pour traverser le pont. Par conséquent, le joueur doit trouver une solution mettant moins de 64 minutes afin de pouvoir traverser le pont, le détruire, et rentrer indemnes.</p>
<p><strong>Solution optimale :</strong> la solution optimale est de faire un voyage avec l’aventurier et l’aventurière, puis un voyage avec le médecin et l’archéologue. Voici un des déroulements possibles de cette solution :</p>
<ol type="1">
<li>aller : aventurière + aventurier = 10 minutes ;</li>
<li>retour : aventurière = 5 minutes ;</li>
<li>aller : médecin + archéologue = 25 minutes ;</li>
<li>retour : aventurier = 10 minutes ;</li>
<li>aller : aventurière + aventurier = 10 minutes.</li>
</ol>
<p>Nous obtenons donc un total de 10 + 5 + 25 + 10 + 10 = 60 minutes, soit 5 minutes de moins qu’avec la solution intuitive.<br />
L’efficacité de cette solution réside dans le fait que l’aventurière donne la torche au médecin et à l’archéologue afin qu’ils voyagent ensemble. Cette subtilité permet de transformer les 2 derniers allers de 20 et 25 minutes par 25 et 10 minutes. En contrepartie, le dernier retour prend 10 minutes au lieu de 5. Cependant, cette perte de temps sur les retours est compensée par le gain de temps sur les allers.</p>
<p><strong>Lien avec l’informatique :</strong> le but de cette activité est d’introduire le concept d’optimisation combinatoire. C’est-à-dire de trouver un ensemble idéal de combinaisons (ou solution idéale) parmi toutes les combinaisons possibles. Pour un être humain, ce problème est abordable avec un ensemble de quatre personnages, cependant si le nombre de personnages augmente, la difficulté augmente grandement, et il devient difficile à appréhender et à résoudre. De plus, bien que l’humain puisse trouver intuitivement une solution satisfaisante, il lui est plus difficile d’en trouver une optimale.<br />
En réalisant un programme adapté et en lui donnant les paramètres nécessaires, l’ordinateur peut nous permettre de trouver plus rapidement cette solution optimale. Ce genre de problème se trouve dans de nombreux domaines tels que l’industrie ou la biologie, l’informatique peut donc y être d’une aide non négligeable.</p>
<iframe width="560" height="315" src="https://www.youtube.com/embed/L-_EFnzREgY" frameborder="0" allowfullscreen>
</iframe>
<h3 id="le-tour-de-magie">Le tour de magie</h3>
<p><strong>Nombre de joueur(s) :</strong> 3 (un magicien, un spectateur et un assistant)</p>
<p><strong>Matériel nécessaire :</strong> 36 tuiles, chaque tuile ayant une couleur différente par face (par exemple : chaque tuile possède une face de couleur rouge et une face de couleur bleue)<br />
Un objet pouvant être caché sous une tuile : le trésor</p>
<p><strong>But du jeu :</strong> le magicien doit trouver le trésor que le spectateur aura caché sous une tuile.</p>
<p><strong>Règles du jeu :</strong></p>
<ul>
<li>l’assistant demande au spectateur de disposer 25 tuiles, recto ou verso, de façon à former un carré de 5 lignes et 5 colonnes ;</li>
<li>l’assistant ajoute une ligne et une colonne supplémentaire en utilisant les tuiles restantes. Chaque ligne et chaque colonne doit contenir un nombre pair de faces de la même couleur (par exemple : 2 faces rouges et 3 faces bleues, en ajoutant une tuile bleue, il y aura donc 2 faces rouges et 4 faces bleues. Le nombre de faces de la même couleur est pair) ;</li>
<li>l’assistant demande au spectateur de cacher le trésor sous une des tuiles de son choix, et de retourner cette dernière après avoir caché le trésor ;</li>
<li>le magicien fait son entrée, il n’a pas assisté aux étapes précédentes et ne sait pas encore où se trouve le trésor. Il doit donc découvrir sous quelle tuile se cache celui-ci.</li>
</ul>
<p><strong>Technique pour retrouver le trésor :</strong> afin de trouver où se cache le trésor, le magicien doit suivre ces étapes :</p>
<ol type="1">
<li>rechercher une ligne dont le nombre de faces de la même couleur est impair ;</li>
<li>rechercher une colonne dont le nombre de faces de la même couleur est impair ;</li>
<li>le trésor se trouve sous la tuile correspondant au croisement entre la ligne et la colonne trouvées plus haut. Le magicien n’a plus qu’à retourner cette tuile.</li>
</ol>
<p><strong>Lien avec l’informatique :</strong> en informatique, l’information est codée sous forme binaire. C’est-à-dire qu’un nombre ou une lettre peut être représentée par un code binaire, une suite de bits (0 ou 1) qui correspond à une valeur donnée (par exemple, la lettre « a » est souvent représentée par le code « 01100001 »).<br />
Les 25 tuiles disposées au début du jeu représentent un message à envoyer. Une tuile représente un bit, et la face de couleur représente la valeur de ce bit (par exemple : rouge = 0, bleu = 1).<br />
Ce message peut être envoyé par Wi-Fi, Bluetooth ou via un câble, il se peut donc qu’une partie du message soit perdue ou dégradée.<br />
Dans cette activité, lorsque le trésor est caché sous une tuile et que cette dernière est retournée, une erreur est introduite dans le message créé plus tôt.<br />
Une des façons possible de détecter la présence d’une erreur est d’ajouter des bits de parité. C’est que qui est fait dans cette activité, lorsque l’assistant ajoute la ligne et la colonne de tuiles supplémentaire. Grâce à ces bits de parité, il est possible de détecter où se trouve l’erreur dans le message (ou bien de trouver le trésor caché).</p>
<iframe width="560" height="315" src="https://www.youtube.com/embed/EALMiiwvYyM" frameborder="0" allowfullscreen>
</iframe>
<div class="signature">
<p>Philippe MARQUET<br />
<a href="http://portail.fil.univ-lille1.fr/mediation/">portail.fil.univ-lille1.fr/mediation/</a></p>
</div>
