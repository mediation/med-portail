<h2 id="e-journée-eia-mars-2019">5e Journée EIA, mars 2019</h2>
<p>Journée enseignement de l’informatique et l’algorithmique, mercredi 6 mars 2019</p>
<p>Une JEAI spéciale IA !</p>
<p>Informations et inscriptions sur <a href="http://jeia.fil.univ-lille1.fr">jeia.fil.univ-lille1.fr</a></p>
<div class="signature">
<p>Philippe MARQUET<br />
<a href="http://portail.fil.univ-lille1.fr/mediation/">portail.fil.univ-lille1.fr/mediation/</a></p>
</div>
