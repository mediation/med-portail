<h2 id="contacts">Contacts</h2>
<ul>
<li>Philippe Marquet<br />
<a href="philippe.marquet@univ-lille.fr">philippe.marquet@univ-lille.fr</a><br />
Bureau 225, 2e étage de l’extension du bâtiment M3<br />
<a href="https://www.cristal.univ-lille.fr/profil/marquet">coordonnées</a></li>
</ul>
<div class="signature">
<p>Philippe MARQUET<br />
<a href="http://portail.fil.univ-lille1.fr/mediation/">portail.fil.univ-lille1.fr/mediation/</a></p>
</div>
