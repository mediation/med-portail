<h2 id="isn---informatique-et-science-du-numérique">ISN - Informatique et science du numérique</h2>
<p>Nous avons assuré la formation des enseignants de la spécialité ISN, <em>Infomatique et science du numérique</em>, de l’académie de Lille.</p>
<p>À cette occasion, nous avons produit un ensemble de propositions d’activités à mener avec les élèves qui couvrent certains points du programme de la spécialité. Retrouvez ces propositions sur</p>
<ul>
<li><a href="http://isn.fil.univ-lille1.fr/">http://isn.fil.univ-lille1.fr/</a></li>
</ul>
<div class="signature">
<p>Philippe MARQUET<br />
<a href="http://portail.fil.univ-lille1.fr/mediation/">portail.fil.univ-lille1.fr/mediation/</a></p>
</div>
