<h2 id="médiation-scientifique-en-informatique">Médiation scientifique en informatique</h2>
<p>0110100101111001… La médiation scientifique en informatique</p>
<p>Vous pouvez contacter :</p>
<ul>
<li><a href="https://www.cristal.univ-lille.fr/profil/marquet">Philippe Marquet</a></li>
</ul>
<p>De nombreux enseignants du département informatique - FIL - participent à ces actions de médiation, ou sont intéressés par cette question. En particulier Laetitia Jourdan et Philippe Marquet ont encadré les étudiants qui ont mis en place les “coding goûters” pour les enfants des personnels d’Inria, de l’université et l’animation tout public des journées portes ouvertes. Philippe Marquet a initié les JEIA. Pierre Boulet, Francesco de Comité, Didier Mailliet, Philippe Marquet, Maude Pupin, Jean-Christophe Routier, Jean-Stéphane Varré, Marie-Emilie Voge, et Éric Wegrzynowski ont contribué aux formations des enseignants d’ISN. etc.</p>
<div class="signature">
<p>Philippe MARQUET<br />
<a href="http://portail.fil.univ-lille1.fr/mediation/">portail.fil.univ-lille1.fr/mediation/</a></p>
</div>
