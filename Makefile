
PAGES = med onglets \
	jpo-cg17 jeia19 isn \
	contact

portail/%.php : src/%.md
	pandoc $< -o $@
portail/%.php : src/%.php
	cp -af $< $@

# install -> ./portail -> git 
.PHONY : install
install : $(addprefix portail/,$(addsuffix .php,$(PAGES)))
	@echo git commit push to git to install ! 
